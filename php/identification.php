<?php
//TODO à = &agrave;  --> ACCENTS

/**
 * Inscription d'un utilisateur
 *
 * @author : Sylvain DUMONTET
 * @author : Berenger OSSETE GOMBE
 */

// Bufferisation des sorties
ob_start();

// Inclusion de la bibliothéque
include('bibli_24sur7.php');



//-----------------------------------------------------
// Détermination de la phase de traitement :
// 1er affichage ou soumission du formulaire
//-----------------------------------------------------
if (! isset($_POST['btnValider']))
    {
        // On n'est dans un premier affichage de la page.
        // => On intialise les zones de saisie.
        $_POST['txtNom'] = $_POST['txtMail'] = '';
        $_POST['txtVerif'] = $_POST['txtPasse'] = '';
    }
else
    {
        // On est dans la phase de connection de l'usr
        // => vérification des valeurs reçues et connection
        // Si aucune erreur n'est détectée, sd_bog_l_connecte_utilisateur()
        // redirige la page sur le script protégé.
        $erreur = !sd_bog_l_connexion_utilisateur_ok();

        if( $erreur === false )
            {
                header('location: agenda.php');
            }
        
    }

//-----------------------------------------------------
// Affichage de la page
//-----------------------------------------------------
echo '<!DOCTYPE HTML>';
echo '<html>';
sd_bog_html_head('24sur7 | Identification');
echo '<body>';
echo '<div id="carnet">';

sd_bog_html_bandeau_sans_onglet();

echo '<main>';
echo '<section id="centre">';

echo '<p>Pour vous connecter, veuillez vous identifier</p>';

// Si il y a des erreurs on les affiche
if (isset($erreur) && $erreur === true)
    {
        echo "<strong>Votre adresse mail ou votre mot de passe est incorrecte.</strong>";
    }

// Affichage du formulaire
echo '<form method="POST" action="identification.php" id="formIdentification" >',
    '<table border="1" cellpadding="4" cellspacing="0">',
    sd_bog_form_ligne('Email',
    sd_bog_form_input(APP_Z_TEXT, 'txtMail', $_POST['txtMail'], 20)),
    sd_bog_form_ligne('Mot de passe',
    sd_bog_form_input(APP_Z_PASS, 'txtPasse', $_POST['txtPasse'], 20)),
    sd_bog_form_ligne(sd_bog_form_input(APP_Z_SUBMIT, 'btnValider', 'S\'identifier'),

    sd_bog_form_input(APP_Z_RESET, 'btnAnnuler', 'Annuler')),
    '</table></form>';


echo '<p>Pas encore de compte ? <a href="../php/inscription.php">Inscrivez-vous</a> sans plus tarder !';
echo '<p>Vous hésitez à vous inscrire ? Laissez-vous séduire par une <a href="../html/presentation.html">une présentation</a> des possibilités de 24sur7';

echo '</section>';
echo '</main>';

ob_end_flush();

sd_bog_html_pied();

echo '</div>'; //fermeture div carnet
echo '</body>';
echo '</html>';

//=================== FIN DU SCRIPT =============================

//_______________________________________________________________
//
//		FONCTIONS LOCALES
//_______________________________________________________________

/**
 * Validation de la saisie et connection d'un utilisateur.
 *
 * Les zones reçues du formulaires de saisie sont vérifiées. Si
 * des erreurs sont détectées elles sont renvoyées sous la forme
 * d'un tableau. Si il n'y a pas d'erreurs, création de la session 
 * utilisateur
 *
 * @global array		$_POST		zones de saisie du formulaire
 *
 * @return array 	Tableau des erreurs détectées
 */
function sd_bog_l_connexion_utilisateur_ok()
{
    sd_bog_bd_connexion();
    
    /* verification presence utilisateur dans la bd */
    /*$S = 'SELECT utiID, utiNom FROM utilisateur WHERE utiMail="'.htmlentities($_POST['txtMail']).'" AND utiPasse="'.md5(htmlentities($_POST['txtPasse'])).'" ';
      TODO: PAS DE MOT DE PASSE REQUIS
     */

    $S = 'SELECT utiID, utiNom FROM utilisateur WHERE utiMail="'.htmlentities($_POST['txtMail']).'"';
    
    $R = mysqli_query($GLOBALS['bd'], $S) or sd_bog_bd_erreur($S);
    $T = mysqli_fetch_assoc($R);
        
    if( !isset($T['utiID']) )
        {
            return false;
        }
    

    /* cela fonctionne */
    session_start();
    
    $_SESSION['id'] = $T['utiID'];
    $_SESSION['nom'] = $T['utiNom'];

    mysqli_close($GLOBALS['bd']);
    mysqli_free_result($R);
    
    return true;
}


?>