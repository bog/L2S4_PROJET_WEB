<?php

/*
TODO les rendez vous -1 (jour entier)
TODO les catégories des personnes suivies
TODO navigation  fleches
TODO fleches pourries
TODO categories de la semaine uniquement
TODO heure minimale et maximale parametre
TODO real_escape pour requetes (SELECT)
 */

/** @file
 * Page d'accueil de l'application 24sur7
 *
 * @author : Sylvain DUMONTET 
 * @author : Berenger OSSETE GOMBE
 */

include('bibli_24sur7.php');	// Inclusion de la bibliothéque

//verification session
sd_bog_verifie_session();

echo '<!DOCTYPE HTML>';
echo '<html>';

sd_bog_html_head('24sur7 | Agenda');

echo '<body>';
echo '<div id="carnet">';

sd_bog_html_bandeau(APP_PAGE_AGENDA);

echo '<main>';

echo '<section id="gauche">';


if( isset($_GET['j']) && isset($_GET['m']) && isset($_GET['a'])
&& is_numeric($_GET['j']) && is_numeric($_GET['m']) && is_numeric($_GET['a']))
    
    {
        $j = htmlentities($_GET['j']);
        if( $j < 10 ){ $j = '0'.(int)$j;}
        $m =  htmlentities($_GET['m']);
        if( $m < 10 ){ $m = '0'.(int)$m;}
        $a =  htmlentities($_GET['a']);
        
        $date = $a.$m.$j;

        sd_bog_html_calendrier($j, $m , $a);
        
    }
else
    {
        sd_bog_html_calendrier();
        $date = "";
    }

sd_bog_html_categorie();

echo '</section>';

sd_bog_l_html_semainier($date);

echo '</main>';

sd_bog_html_pied();

echo '</div>'; //fermeture bloc carnet
echo '</body>';
echo '</html>';





function sd_bog_l_html_semainier($date_demande)
{

    $jour_intitule = array('lundi', 'mardi', 'mercredi', 'jeudi', 'vendredi', 'samedi', 'dimanche');
    $mois_intitule = array('', 'janvier', 'f&eacute;vrier', 'mars', 'avril', 'mai', 'juin', 'juillet', 'ao&ucirc;t', 'septembre', 'octobre', 'novembre', 'd&eacute;cembre');
    
    $nb_jour_semaine = 7;

    
    /* DETERMINATION SEMAINE A AFFICHER */
    $j_demande = substr($date_demande, 6, 8);
    $m_demande = substr($date_demande, 4, -2);
    $a_demande = substr($date_demande, 0, 4);

    if ((!checkdate($m_demande, $j_demande, $a_demande)) || $a_demande < 1000 || $a_demande > 9999)
        {
            $j_demande= date('j',time());
            $m_demande= date('n',time());
            $a_demande= date('Y',time());
        }

    $jour_semaine = date('w',mktime(0,0,0,$m_demande,$j_demande,$a_demande)) - 1;

    //dimanche.
    if( $jour_semaine == -1 )
        {
            $lundi_date = mktime(0,0,0,$m_demande,$j_demande - 6, $a_demande);
            $dimanche_date = mktime(0,0,0,$m_demande, $j_demande, $a_demande);
        }
    //lundi etc.
    else
        {
            $lundi_date = mktime(0,0,0,$m_demande, $j_demande - $jour_semaine, $a_demande);
            $dimanche_date = mktime(0,0,0,$m_demande, ($j_demande - $jour_semaine) + 6,$a_demande);
        }


    $lundi = date('Y', $lundi_date).date('m',$lundi_date).date('d',$lundi_date);
    
    $dimanche = date('Y', $dimanche_date).date('m',$dimanche_date).date('d',$dimanche_date);


    $j_lundi = substr($lundi, 6, 8);
    $m_lundi = $mois_intitule[(int)substr($lundi, 4, -2)];

    $j_dimanche = substr($dimanche, 6, 8);
    $m_dimanche = $mois_intitule[(int)substr($dimanche, 4, -2)];
    
    /*
    echo '<script language="JavaScript">',
        'alert(" Lundi: '.$lundi.' dimanche: '.$dimanche.'")',
        '</script>';

        DEBUG affiche lundi et dimanche
    */

    
    /* CONNEXION BD */
    ob_start();
    sd_bog_bd_connexion();
    
    //determination heure maximum et minimum de l'utilisateur
    $S = 'SELECT utiHeureMin, utiHeureMax 
FROM utilisateur WHERE utiID = "'.mysqli_real_escape_string($GLOBALS['bd'], $_SESSION['id']).'" ';
    $R = mysqli_query($GLOBALS['bd'], $S) or sd_bog_bd_erreur($S);
    $T = mysqli_fetch_assoc($R);
    
    $heure_min_uti = (int)$T['utiHeureMin'];
    $heure_max_uti = (int)$T['utiHeureMax'];
    
    //determination de l'heure minimum
    $S = 'SELECT MIN(DISTINCT rdvHeureDebut) 
FROM rendezvous
WHERE rdvIDUtilisateur = '.htmlentities($_SESSION['id']).'
AND rdvHeureDebut <> -1';
    $R = mysqli_query($GLOBALS['bd'], $S) or sd_bog_bd_erreur($S);
    $T = mysqli_fetch_assoc($R);
    //l'affichage des journees commence au moins a 8h
    $heure_debut = min($heure_min_uti, $T['MIN(DISTINCT rdvHeureDebut)']/100); 

    //determination de l'heure maximale TODO : heure max de la SEMAINE UNIQUEMENT
    $S = 'SELECT MAX(DISTINCT rdvHeureFin) 
FROM rendezvous
WHERE rdvIDUtilisateur = '.htmlentities($_SESSION['id']);
    $R = mysqli_query($GLOBALS['bd'], $S) or sd_bog_bd_erreur($S);
    $T = mysqli_fetch_assoc($R);
    //l'affichage des journees finit au moins a 16h
    $heure_fin = max($heure_max_uti, ceil(((float)$T['MAX(DISTINCT rdvHeureFin)'])/100.0));

    //determination du nombre d'heures
    $nb_heure = $heure_fin - $heure_debut;
    
    $hauteur_case = 40; // cf css

   

    /* ENTETE */
    echo '<section id="droite">';
    echo '
<p id="titreAgenda">

<a href="" ><img class="fleche_gauche" alt="precedent" src="../images/fleche_gauche.png" /></a>
	  <strong>Semaine du '.$j_lundi.' '.$m_lundi.' au '.$j_dimanche.' '.$m_dimanche.'</strong> pour <strong>'.$_SESSION['nom'].'</strong>
	  <a href=""><img class="fleche_droite" alt="suivant" src="../images/fleche_droite.png" /></a> 

</p>';// TODO fleche degeulasse
    // Mois identique: affiche moins crado

    
    
    echo '<section id="agenda">';


    // savoir si on a une journee entiere en semaine
    $journee_entiere = true;
    $journee_entiere_hauteur = 30;
    
    $S = 'SELECT MIN(DISTINCT rdvHeureDebut) 
FROM rendezvous
WHERE rdvIDUtilisateur = '.mysqli_real_escape_string($GLOBALS['bd'], $_SESSION['id']).' AND rdvDate >= "'.mysqli_real_escape_string($GLOBALS['bd'],$lundi).'" AND rdvDate <= "'.mysqli_real_escape_string($GLOBALS['bd'],$dimanche).'"';


    $R = mysqli_query($GLOBALS['bd'], $S) or sd_bog_bd_erreur($S);
    $T = mysqli_fetch_assoc($R);
    $journee_entiere = ( htmlentities($T['MIN(DISTINCT rdvHeureDebut)']) === '-1' );
    
    /* COLONNE DES HORAIRES */
   
    
    echo '<aside style="margin-top:'.$journee_entiere * $journee_entiere_hauteur.'px;" id="horaire">';

    for($i=$heure_debut; $i <= $heure_fin; $i++)
        {
            echo '<p>'.$i.'h</p>';
        }
    echo '</aside>'; // fermeture aside horaire
    
    /* NOM JOURS */
    echo '<div id="jourEntete">';
    for($j=0; $j<$nb_jour_semaine;$j++)
        {
            echo '<div class="jourNom" >'.$jour_intitule[$j].'</div>';
        }
    echo '</div>';
    
    
        

    /* JOURS SEMAINIER */
    
    for($j=0; $j<$nb_jour_semaine;$j++)
        {
            /* jours */
            echo '<div class="jour" style="margin-top: '.$journee_entiere * $journee_entiere_hauteur.'px;height:'.(($nb_heure) * ($hauteur_case+2) - 2).'px"';

            if( $j === $nb_jour_semaine-1 )
                {
                    echo ' id="dimanche"';
                }
            echo '>';

            /* cases heures */
            for( $h=0; $h< $nb_heure; $h++)
                {

                    $current_date = mktime(0,0,0,$m_demande,$j_lundi + $j,$a_demande);
                    $date_rdv = date('Y', $current_date).date('m', $current_date).date('d', $current_date);
                    
                    echo '<div class="caseHeure">';
                    echo '<a href="rendezvous.php?d='.$date_rdv.'&h='.(($heure_debut+$h)*100).'"></a>';
                    echo '</div>';
                }
            echo '</div>';
        }


    /* PLAGES HORAIRES */
    //plage dans la base

    /* journee entiere */
    $S = 'SELECT * FROM rendezvous, utilisateur, categorie
       WHERE utiID = "'.mysqli_real_escape_string($GLOBALS['bd'],$_SESSION['id']).'" 
AND utiID = rdvIDUtilisateur
       AND rdvDate >= "'.mysqli_real_escape_string($GLOBALS['bd'],$lundi).'"
 AND rdvDate <= "'.mysqli_real_escape_string($GLOBALS['bd'],$dimanche).'" 
AND rdvIDCategorie = catID';
    
    $R = mysqli_query($GLOBALS['bd'], $S) or sd_bog_bd_erreur($S);

    while( $T = mysqli_fetch_assoc($R) )
        {

            $distance_a_lundi = date('w', mktime(0,0,0,
            substr($T['rdvDate'],4, -2),
            substr($T['rdvDate'],6, 8),
            substr($T['rdvDate'],0, 4) )) - 1;
            //dimanche
            if( $distance_a_lundi === -1 )
                {
                    $distance_a_lundi = 6;
                }

            $heure_fin_minute = (int)($T['rdvHeureFin']/100)*60 + $T['rdvHeureFin']%100;
            $heure_debut_minute = (int)($T['rdvHeureDebut']/100)*60 + $T['rdvHeureDebut']%100;
            $duree_heure = ($heure_fin_minute - $heure_debut_minute)/60;

            $distance_debut_jour = $heure_debut_minute/60 - $heure_debut;
                

                    
            if( htmlentities($T['rdvHeureDebut']) === '-1' &&
            htmlentities($T['rdvHeureFin']) === '-1')
                {
                    $style_rdv = ' position: absolute;
width:90px;
 height:'.( $journee_entiere_hauteur).'px;
color: #'.$T['catCouleurFond'].';
background-color: #'.$T['catCouleurFond'].';
border: solid 2px #'.$T['catCouleurBordure'].'; 

left: '.(43  + $distance_a_lundi * 97.75).'px;
top: '.(33).'px;';
                        }
            else
                {
            
            //TODO (fignolage) hauteur un peu moins degeulasse
            
            $style_rdv = ' position: absolute;
width:90px;
 height:'.($duree_heure * 42 - 8).'px;
color: #'.$T['catCouleurFond'].';
background-color: #'.$T['catCouleurFond'].';
border: solid 2px #'.$T['catCouleurBordure'].'; 

left: '.(43  + $distance_a_lundi * 97.75).'px;
top: '.(43 + $distance_debut_jour * 42 + $journee_entiere * $journee_entiere_hauteur ).'px;

';// TODO -1 dans la hauteur, degeulasse !
                }
            
            echo '<div style="'.$style_rdv.'">';
            echo '<a href="rendezvous.php?id='.$T['rdvID'].'">'.$T['rdvLibelle'].'</a>';
            echo '</div>';//TODO troncage degeulasse
        }
    
    echo '</section>'; //fermeture section agenda
    echo '</section>'; //fermeture section droite


    //fermeture bd
    mysqli_close($GLOBALS['bd']);
    mysqli_free_result($R);
}




?>