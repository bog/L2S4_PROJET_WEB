<?php

function sd_html_head($titre, $css='../styles/style.css'){
	echo'<!DOCTYPE HTML>',
		'<html>',
			'<head>',
				'<meta charset="ISO-8859-1">',
				'<title>24sur7 | Votre agenda</title>',
				'<link rel="stylesheet" href="../styles/style.css" type="text/css">',
				'<link rel="shortcut icon" href="../images/favicon.ico" type="image/x-icon">',
			'</head>',
			'<body>',
				'<main id="bcPage">';
}

function sd_html_bandeau($page){

	echo'<header id="bcEntete">',
		'<div id="bcLogo"></div>',
			'<nav id="bcOnglets">';

	switch ($page) {
		case 1:
			echo
				'<h2>Agenda</h2>',
				'<a href="#">Recherche</a>',
				'<a href="#">Abonnements</a>',
				'<a href="#">Param&egrave;tres</a>';
			break;
		case 2:
			echo
				'<a href="#">Agenda</a>',
				'<h2>Recherche</h2>',
				'<a href="#">Abonnements</a>',
				'<a href="#">Param&egrave;tres</a>';
			break;
		case 3:
			echo
				'<a href="#">Agenda</a>',
				'<a href="#">Recherche</a>',
				'<h2>Abonnements</h2>',
				'<a href="#">Param&egrave;tres</a>';
			break;
		case 4:
			echo
				'<a href="#">Agenda</a>',
				'<a href="#">Recherche</a>',
				'<a href="#">Abonnements</a>',
				'<h2>Param&egrave;tres</h2>';
			break;
		default:
			echo "Erreur ! Page inconnue.";
			return 1;
	}
	echo
		'</nav>',
		'<a href="#" id="btnDeconnexion" title="Se d&eacute;connecter"></a>',
		'</header>';
}

function sd_html_pied(){
	echo
				'<footer id="bcPied">',
					'<a id="apropos" href="#">A propos</a>',
					'<a id="confident" href="#">Confidentialit&eacute;</a>',
					'<a id="conditions" href="#">Conditions</a>',
					'<p id="copyright">24sur7 &amp; Partners &copy; 2012</p>',
				'</footer>',
			'</main>',
		'</body>',
	'</html>';
}

function sd_html_contenu($page){
	switch ($page) {
		case 1:
			sd_html_contenu_agenda();
			break;
		case 2:
			sd_html_contenu_recherche();
			break;
		case 3:
			sd_html_contenu_abonnements();
			break;
		case 4:
			sd_html_contenu_parametres();
			break;
		default:
			echo "Erreur ! Page inconnue.";
			return 1;
	}
}

function sd_html_contenu_agenda(){
	echo
		'<section id="bcContenu">',
		'<aside id="bcGauche">';

	# Appel la fonction créant le bloc calendrier avec (jour, mois, année) en paramètres
	sd_html_calendrier();
	echo
		'<section id="categories">Ici : bloc catégories pour afficher les catégories de rendez-vous</section>',
		'</aside>',
		'<section id="bcCentre">Ici : bloc avec le détail des rendez-vous de la semaine du 9 au 15 février 2015',
		'</section>',
		'</section>';
}

function sd_html_calendrier($jour=0, $mois=0, $annee=0){
	# vérification des paramètres #
	$param_defaut = false;
	$t=time();
	if ((!checkdate($mois, $jour, $annee))|| ($annee<2012)) {
		$jour= date('j',$t);
		$mois= date('n',$t);
		$annee= date('Y',$t);
	}

	# tableau des jours
	$nom_jour = array('dimanche', 'lundi', 'mardi', 'mercredi', 'jeudi', 'vendredi', 'samedi');

	# tableau des mois
	$nom_mois = array('', 'janvier', 'f&eacute;vrier', 'mars', 'avril', 'mai', 'juin', 'juillet', 'ao&ucirc;t', 'septembre', 'octobre', 'novembre', 'd&eacute;cembre');

	#titre calendrier + en-tête tableau
	echo
		'<section id="calendrier">',
		'<p>',
		'<a href="#" class="flechegauche"><img src="../images/fleche_gauche.png" alt="picto fleche gauche"></a>',
		$nom_mois[$mois],' ',$annee,
		'<a href="#" class="flechedroite"><img src="../images/fleche_droite.png" alt="picto fleche droite"></a>',
		'</p>',
		'<table>',
		'<tr>',
		'<th>Lu</th><th>Ma</th><th>Me</th><th>Je</th><th>Ve</th><th>Sa</th><th>Di</th>',
		'</tr>';

	#détermine les variables necessitant un appel à date()
	$premier_j=date('N',mktime(0,0,0,$mois,1,$annee));
	$nbj_mois_courant=date('t',mktime(0,0,0,$mois,$jour,$annee));
	$nbj_mois_avant=date('t',mktime(0,0,0,$mois-1,$jour,$annee));
	$aujourdhui=mktime(0,0,0,date('n',$t),date('j',$t),date('Y',$t));
	$jour_select=mktime(0,0,0,$mois,$jour,$annee);
	$semaine_select=date('W',mktime(0,0,0,$mois,$jour,$annee));

	#compte le nombre de lignes necessaires dans le calendrier
	if ((($nbj_mois_courant+$premier_j-1)/7)<=5) {
		$nb_lignes=5;
	}
	else {
		$nb_lignes=6;
	}

	# Ecrit le contenu du tableau
	for ($i=0;$i<$nb_lignes;$i++){
		if ($semaine_select==date('W',mktime(0,0,0,$mois,(1+$i*7),$annee))) {
			echo '<tr class="semaineCourante">';
		}
		else {
			echo'<tr>';
		}
		for ($j=1;$j<8;$j++){
			if (($i*7+$j)<$premier_j) { # si case dans le mois précédent
				echo'<td><a class="lienJourHorsMois" href="#">',($nbj_mois_avant-$premier_j+$j+1),'</a></td>';
			}
			else {
				if (($i*7+$j)>=($premier_j+$nbj_mois_courant)) { # si case dans le mois suivant
					echo'<td><a class="lienJourHorsMois" href="#">',($i*7+$j-$nbj_mois_courant-$premier_j+1),'</a></td>';
				}
				else {
					if ($aujourdhui==mktime(0,0,0,$mois,($i*7+$j-$premier_j+1),$annee)) { # si case correspond à aujourd'hui
						echo'<td><a class="aujourdHui" href="#">',($i*7+$j-$premier_j+1),'</a></td>';
					}
					else {
						if (mktime(0,0,0,$mois,($i*7+$j-$premier_j+1),$annee)==$jour_select) { # si case correspond au jour sélectionné
							echo'<td><a class="jourCourant" href="#">',($i*7+$j-$premier_j+1),'</a></td>';
						}
						else { # sinon (case correspond au mois courant, mais pas à aujourd'hui ni au jour sélectionné)
							echo'<td><a href="#">',($i*7+$j-$premier_j+1),'</a></td>';
						}
					}
				}
			}
		}
		echo'</tr>';
	}
	echo
		'</table>',
		'</section>';
}

function sd_html_contenu_recherche(){
}

function sd_html_contenu_abonnements(){
}

function sd_html_contenu_parametres(){
}




?>

