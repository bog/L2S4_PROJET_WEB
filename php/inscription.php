<?php
//TODO à = &agrave;  --> ACCENTS

/**
 * TP 5 : Inscription d'un utilisateur
 *
 * @author : Sylvain DUMONTET
 * @author : Berenger OSSETE GOMBE
 */

// Bufferisation des sorties
ob_start();

// Inclusion de la bibliothéque
include('bibli_24sur7.php');



//-----------------------------------------------------
// Détermination de la phase de traitement :
// 1er affichage ou soumission du formulaire
//-----------------------------------------------------
	if (! isset($_POST['btnValider'])) {
		// On n'est dans un premier affichage de la page.
		// => On intialise les zones de saisie.
		$nbErr = 0;
		$_POST['txtNom'] = $_POST['txtMail'] = '';
		$_POST['txtVerif'] = $_POST['txtPasse'] = '';
	} else {
		// On est dans la phase de soumission du formulaire :
		// => vérification des valeurs reçues et création utilisateur.
		// Si aucune erreur n'est détectée, sd_bog_l_add_utilisateur()
		// redirige la page sur le script protégé.
		$erreurs = sd_bog_l_add_utilisateur();
		$nbErr = count($erreurs);
	}

//-----------------------------------------------------
// Affichage de la page
//-----------------------------------------------------

echo '<!DOCTYPE HTML>';
echo '<html>';

sd_bog_html_head('24sur7 | Inscription');
echo '<body>';
echo '<div id="carnet">';

sd_bog_html_bandeau_sans_onglet();

echo '<main>',
    '<section id="centre" >';

echo '<p>Pour vous inscrire à <strong>24sur7</strong>, veuillez remplir le formulaire ci-dessous.</p>';

// Si il y a des erreurs on les affiche
if ($nbErr > 0) {
    echo '<strong>Les erreurs suivantes ont été détectées</strong>';
    for ($i = 0; $i < $nbErr; $i++) {
        echo '<br>', $erreurs[$i];
    }
}

// Affichage du formulaire
echo '<form method="POST" action="inscription.php" id="formInscription">',
    '<table border="1" cellpadding="4" cellspacing="0">',
    sd_bog_form_ligne('Indiquez votre nom',
    sd_bog_form_input(APP_Z_TEXT, 'txtNom', $_POST['txtNom'], 20)),
    sd_bog_form_ligne('Indiquez une adresse mail valide',
    sd_bog_form_input(APP_Z_TEXT, 'txtMail', $_POST['txtMail'], 20)),
    sd_bog_form_ligne('Choisissez un mot de passe',
    sd_bog_form_input(APP_Z_PASS, 'txtPasse', $_POST['txtPasse'], 20)),
    sd_bog_form_ligne('Répétez votre mot de passe',
    sd_bog_form_input(APP_Z_PASS, 'txtVerif', $_POST['txtVerif'], 20)),

    sd_bog_form_ligne(sd_bog_form_input(APP_Z_SUBMIT, 'btnValider', 'S\'inscrire'),

    sd_bog_form_input(APP_Z_RESET, 'btnAnnuler', 'Annuler')),
    '</table></form>';


echo '<p>Déjà inscrit ? <a href="../php/identification.php">Identifiez-vous !</a>';
echo '<p>Vous hésitez à vous inscrire ? Laissez-vous séduire par une <a href="../html/presentation.html">une présentation</a> des possibilités de 24sur7';

echo '</section>';
echo '</main>';

ob_end_flush();

sd_bog_html_pied();

echo '</div>'; //fermeture bloc carnet
echo '</body>';
echo '</html>';

//=================== FIN DU SCRIPT =============================

//_______________________________________________________________
//
//		FONCTIONS LOCALES
//_______________________________________________________________

/**
 * Validation de la saisie et création d'un nouvel utilisateur.
 *
 * Les zones reçues du formulaires de saisie sont vérifiées. Si
 * des erreurs sont détectées elles sont renvoyées sous la forme
 * d'un tableau. Si il n'y a pas d'erreurs, un enregistrement est
 * créé dans la table utilisateur, une session est ouverte et une
 * redirection est effectuée.
 *
 * @global array		$_POST		zones de saisie du formulaire
 *
 * @return array 	Tableau des erreurs détectées
 */
function sd_bog_l_add_utilisateur() {
    //-----------------------------------------------------
    // Vérification des zones
    //-----------------------------------------------------
    $erreurs = array();

    // Vérification du nom
    $txtNom = trim($_POST['txtNom']);
    $long = strlen($txtNom);
    if ($long < 4
    || $long > 30)
		{
			$erreurs[] = 'Le nom doit avoir de 4 à 30 caractères';
		}

    // Vérification du mail
    $txtMail = trim($_POST['txtMail']);
    if ($txtMail == '')
        {
            $erreurs[] = 'L\'adresse mail est obligatoire';
        }
    elseif (strlen($txtMail) < 4 ||strpos($txtMail, '@') === FALSE || strpos($txtMail, '.') === FALSE)
        {
            $erreurs[] = 'L\'adresse mail n\'est pas valide';
        }
    else
        {
            // Vérification que le mail n'existe pas dans la BD
            sd_bog_bd_connexion();

            $mail = mysqli_real_escape_string($GLOBALS['bd'],$txtMail);

            $S = "SELECT	count(*)
					FROM	utilisateur
					WHERE	utiMail = '$mail'";

            $R = mysqli_query($GLOBALS['bd'], $S) or sd_bog_db_err($S);

            $D = mysqli_fetch_row($R);

            if ($D[0] > 0) {
                $erreurs[] = 'Cette adresse mail est déjà inscrite.';
            }
    }

    // Vérification du mot de passe
    $txtPasse = trim($_POST['txtPasse']);
    $long = strlen($txtPasse);
    if ($long < 4 || $long > 20)
		{
			$erreurs[] = 'Le mot de passe doit avoir de 4 à 20 caractères';
		}

    $txtVerif = trim($_POST['txtVerif']);
    if ($txtPasse != $txtVerif) {
        $erreurs[] = 'Le mot de passe est différent dans les 2 zones';
    }

    // Si il y a des erreurs, la fonction renvoie le tableau d'erreurs
    if (count($erreurs) > 0) {
        return $erreurs;		// RETURN : des erreurs ont été détectées
    }

    //-----------------------------------------------------
    // Insertion d'un nouvel utilisateur dans la base de données
    //-----------------------------------------------------
    $txtPasse = mysqli_real_escape_string($GLOBALS['bd'],md5($txtPasse));
    $nom = mysqli_real_escape_string($GLOBALS['bd'],$txtNom);
    $txtMail = mysqli_real_escape_string($GLOBALS['bd'],$txtMail);
    $utiDateInscription = date('Ymd');

    $S = "INSERT INTO utilisateur SET
				utiNom = '$nom',
				utiPasse = '$txtPasse',
				utiMail = '$txtMail',
				utiDateInscription = $utiDateInscription,
				utiJours = 127,
				utiHeureMin = 6,
				utiHeureMax = 22";

    $R = mysqli_query($GLOBALS['bd'], $S) or sd_bog_bd_erreur($S);

    //-----------------------------------------------------
    // Ouverture de la session et redirection vers la page protégée
    //-----------------------------------------------------
    session_start();
    $_SESSION['utiID'] = mysql_insert_id();
    $_SESSION['utiNom'] = $txtNom;

    header ('location: ../index.php');

    mysqli_close($GLOBALS['bd']);
    mysqli_free_result($R);

    exit();			// EXIT : le script est terminé
}

?>