<?php
// TODO include -> require
// TODO verif uti possede categories !
// TODO phpdoc

/**
 * Modification et ajout d'un RDV
 *
 * @author : Sylvain DUMONTET
 * @author : Berenger OSSETE GOMBE
 */

// Bufferisation des sorties
ob_start();

// Inclusion de la bibliothéque
include('bibli_24sur7.php');
//verification session
sd_bog_verifie_session();


echo '<!DOCTYPE HTML>';
echo '<html>';
sd_bog_html_head('24sur7 | Rendez-Vous');
echo '<body>';
echo '<div id="carnet">';

sd_bog_html_bandeau("");

echo '<main>';

echo '<section id="gauche">';
{
    sd_bog_html_calendrier();
    sd_bog_html_categorie();
}
echo '</section>';

echo '<section id="droite">';



//-----------------------------------------------------
// Détermination de la phase de traitement :
// 1er affichage ou soumission du formulaire
//-----------------------------------------------------

//init valeur par défaut
if( !isset($_POST['btnMaj']) && !isset($_POST['btnAjouter']) )
    {
        $_POST['txtNom'] = '';
    }

/* Traitement mise a jour */
if( isset($_POST['btnMaj']) )
    {
        $date_insertion = (int)$_POST['selDate_a']*10000 + 100*(int)$_POST['selDate_m']
            + (int)$_POST['selDate_j'];

        $horaire_debut_insertion =  (int)$_POST['selHoraireDebut_heure']*100 + (int)$_POST['selHoraireDebut_minute'];
        $horaire_fin_insertion =  (int)$_POST['selHoraireFin_heure']*100 + (int)$_POST['selHoraireFin_minute'];

        if( isset($_POST['checkJourneeEntiere']) )
            {
                $horaire_debut_insertion = -1;
                $horaire_fin_insertion = -1;
            }
        
        $erreurs=sd_bog_l_modif_rdv($_POST['hidden_rdvID'], $_POST['txtNom'], $date_insertion, (int)$_POST['selCategorie'], $horaire_debut_insertion, $horaire_fin_insertion);
        
        
        
        $GLOBALS['rdvModification'] = true;

        //il y a des erreurs
        if( count($erreurs) > 0 )
            {
                foreach($erreurs as $e)
                    {
                        echo '<p>',$e,'</p>';
                    }
            }
        //pas d'erreur
        else
            {
                header('Location: agenda.php');
            }
    }

/* Traitement suppression */
if( isset($_POST['btnSupprimer']) ) // TODO+ msg confirmation
    {
        $erreurs = sd_bog_l_supprimer( $_POST['hidden_rdvID'] );

        //il y a des erreurs
        if( count($erreurs) > 0 )
            {
                foreach($erreurs as $e)
                    {
                        echo '<p>',$e,'</p>';
                    }
            }
         //pas d'erreur
        else
            {
                header('Location: agenda.php');
            }
    }

/* Traitement ajout */
if( isset($_POST['btnAjouter']) )
    {
        
        $date_insertion = (int)$_POST['selDate_a']*10000 + 100*(int)$_POST['selDate_m']
            + (int)$_POST['selDate_j'];

        $horaire_debut_insertion =  (int)$_POST['selHoraireDebut_heure']*100 + (int)$_POST['selHoraireDebut_minute'];
        $horaire_fin_insertion =  (int)$_POST['selHoraireFin_heure']*100 + (int)$_POST['selHoraireFin_minute'];

        if( isset($_POST['checkJourneeEntiere']) )
            {
                $horaire_debut_insertion = -1;
                $horaire_fin_insertion = -1;
            }
        
        $erreurs = sd_bog_l_ajout_rdv($_POST['txtNom'], $date_insertion, (int)$_POST['selCategorie'], $horaire_debut_insertion, $horaire_fin_insertion);
        
        $GLOBALS['rdvModification'] = false;

        //il y a des erreurs
        if( count($erreurs) > 0 )
            {
                foreach($erreurs as $e)
                    {
                        echo '<p>',$e,'</p>';
                    }
            }
        //pas d'erreur
        else
            {
                header('Location: agenda.php');
            }
    }





//-----------------------------------------------------
// Affichage de la page
//-----------------------------------------------------

/* Premier affichage du formulaire */
if( !isset($_POST['btnMaj']) && !isset($_POST['btnAjouter']) )
    {
// TODO debug 
/* PARAMETRE PAR DEFAUT */
$libelle = "rendez-vous";
$date = date('Y',time()).date('m',time()).date('d',time());
$horaire_debut = 800;
$horaire_fin = 900;

$categorie = "";
$journee_entiere = false;

// cas 1 : case libre
if( isset($_GET['d']) && isset($_GET['h']) && is_numeric($_GET['h']) && is_numeric($_GET['d']) )
    {
        $date = htmlentities($_GET['d']);
        $horaire_debut = htmlentities($_GET['h']);
        $horaire_fin = $horaire_debut + 100;

        $libelle = "Nouveau rendez-vous";

         
        $categorie = "";
        $journee_entiere = false;

        $GLOBALS['rdvModification'] = false;
    }

// cas 2 : case rdv
if( isset($_GET['id']) && is_numeric($_GET['id']) )
    {
        ob_start();
        sd_bog_bd_connexion();

        $rdvID = htmlentities($_GET['id']);
        
        $S = '
SELECT *
FROM rendezvous, utilisateur, categorie
 WHERE rdvIDUtilisateur = "'.$_SESSION['id'].'" AND rdvID = "'.$rdvID.'"
AND catID = rdvIDCategorie';

        
        
        $R = mysqli_query($GLOBALS['bd'], $S) or sd_bog_bd_erreur($S);
        $T = mysqli_fetch_assoc($R);
        
        $date = $T['rdvDate'];
        $horaire_debut = $T['rdvHeureDebut'];
        $horaire_fin = $T['rdvHeureFin'];
        
        $libelle = $T['rdvLibelle'];
        $categorie = $T['catNom'];
        $journee_entiere = ($horaire_debut === '-1');
        
        ob_end_flush();
        mysqli_free_result($R);
        
        $GLOBALS['rdvModification'] = true;
    }
else
    {
        $rdvID = 0;
    }

//mauvais id
if( isset($_GET['id']) && !is_numeric($_GET['id']) )
    {
        header('Location: agenda.php');
    }


$jour = substr($date, 6, 8);
$mois = substr($date, 4, -2);
$annee = substr($date, 0, 4);

$horaire_debut_heure = (int)($horaire_debut / 100);
$horaire_debut_minute = $horaire_debut%100;
$horaire_fin_heure = (int)($horaire_fin / 100);
$horaire_fin_minute = $horaire_fin%100;

$mode_modification = (bool)$GLOBALS['rdvModification'];

}
else // TODO : recup horaires & categorie
    {
        $libelle = $_POST['txtNom'];
        $jour  = $_POST['selDate_j'];
        $mois = $_POST['selDate_m'];
        $annee = $_POST['selDate_a'];
        $horaire_debut_heure = $_POST['selHoraireDebut_heure'];
        $horaire_debut_minute = $_POST['selHoraireDebut_minute'];
        $horaire_fin_heure = $_POST['selHoraireFin_heure'];
        $horaire_fin_minute = $_POST['selHoraireFin_minute'];
        $journee_entiere = isset($_POST['checkJourneeEntiere']);
        if( !isset($GLOBALS['rdvModification']) )
            {
                $mode_modification = false;
            }
        else
            {
                $mode_modification = (bool)$GLOBALS['rdvModification'];
            }
        
    }



/* AFFICHAGE */

if( $mode_modification == false )
    {
        echo '<h2>Nouvelle saisie</h2>'; //TODO choix
    }
else
    {
        echo '<h2>Modification</h2>'; //TODO choix
    }

// Affichage du formulaire

echo '<form method="POST" action="rendezvous.php" id="formRdv">',
    '<table border="1" cellpadding="4" cellspacing="0">';

echo '<input type=hidden name=\'hidden_rdvID\', value=\'', $rdvID,'\' />';
    

echo sd_bog_form_ligne('Libellé : ',
    sd_bog_form_input(APP_Z_TEXT, 'txtNom', $_POST['txtNom'], 20)),
    sd_bog_form_ligne('Date : ', sd_bog_form_date('selDate',$jour,$mois,$annee)),

    sd_bog_form_ligne('Catégorie : ',
    sd_bog_form_selection_categories('selCategorie')),

    sd_bog_form_ligne('Horaire début : ',
    sd_bog_form_selection_horaire('selHoraireDebut',$horaire_debut_heure,$horaire_debut_minute)),

    sd_bog_form_ligne('Horaire fin : ',
    sd_bog_form_selection_horaire('selHoraireFin',$horaire_fin_heure,$horaire_fin_minute)),
    
    sd_bog_form_ligne('Ou',
    sd_bog_form_checkbox('checkJourneeEntiere', array(' Événement sur une journée'), array($journee_entiere)) );


if( $mode_modification === true )
    {
        echo sd_bog_form_ligne(sd_bog_form_input(APP_Z_SUBMIT, 'btnMaj', 'Mettre à jour'),
        sd_bog_form_input(APP_Z_SUBMIT, 'btnSupprimer', 'Supprimer'));
    }
else
    {
        echo sd_bog_form_ligne(sd_bog_form_input(APP_Z_SUBMIT, 'btnAjouter', 'Ajouter'),
        sd_bog_form_input(APP_Z_RESET, 'btnAnnuler', 'Annuler'));


    }

echo '</table></form>';

echo '<a href = "agenda.php">Retour à l\'agenda</a>';// TODO utiliser date pour retour
echo '</section>';//fermeture section centre
echo '</main>';


ob_end_flush();
sd_bog_html_pied();

echo '</div>'; //fermeture div carnet
echo '</body>';
echo '</html>';



/* FONCTIONS LOCALES */

function sd_bog_l_ajout_rdv($libelle, $date, $categorie, $horaire_debut, $horaire_fin)
{
    
   $erreurs = sd_bog_l_erreur($libelle, $date, $categorie, $horaire_debut, $horaire_fin);
   
    //insertion
    if( count($erreurs) === 0 )
        {
            sd_bog_bd_connexion();
            
            $S = 'INSERT INTO 
rendezvous (rdvDate, rdvHeureDebut, rdvHeureFin, rdvLibelle, rdvIDCategorie, rdvIDUtilisateur) 
VALUES ( \''.mysqli_real_escape_string($GLOBALS['bd'], $date).'\', \''.mysqli_real_escape_string($GLOBALS['bd'], $horaire_debut).'\', \''.mysqli_real_escape_string($GLOBALS['bd'], $horaire_fin).'\', \''.mysqli_real_escape_string($GLOBALS['bd'], $libelle).'\',\''.mysqli_real_escape_string($GLOBALS['bd'], $categorie).'\',\''.mysqli_real_escape_string($GLOBALS['bd'], $_SESSION['id']).'\'  )';

            $R = mysqli_query($GLOBALS['bd'], $S) or sd_bog_bd_erreur($S);

        }
    
    
    return $erreurs;
}


function sd_bog_l_erreur($libelle, $date, $categorie, $horaire_debut, $horaire_fin)
{
    $erreurs = array();
    
    /* date */
    if( !is_numeric($date) )
        {
            $erreurs['err_date_format'] = 'E: La date est au mauvais format !';            
        }

   $jour = substr($date, 6, 8);
   $mois = substr($date, 4, -2);
   $annee = substr($date, 0, 4);

   if( !checkdate($mois, $jour, $annee) )
       {
           $erreurs['err_date_invalide'] = 'E: La date est invalide !';
       }

   /* horaire debut */
   if( !is_numeric($horaire_debut) )
        {
            $erreurs['err_horaire_debut_format'] = 'E: L\'horaire de début est au mauvais format !';            
        }

   $h = (int)($horaire_debut/100);
   $m = (int)($horaire_debut%100);
   
   if( $h < 0 || $h > 23 || $m < -1 || $m > 59 )
        {
            $erreurs['err_horaire_debut_invalide'] = 'E: L\'horaire de début est invalide !';            
        }
   
/* horaire fin */
   if( !is_numeric($horaire_fin) )
        {
            $erreurs['err_horaire_fin_format'] = 'E: L\'horaire de fin est au mauvais format !';            
        }
   
   $h = (int)($horaire_fin/100);
   $m = (int)($horaire_fin%100);
   
   if( $h < 0 || $h > 23 || $m < -1 || $m > 59 )
        {
            $erreurs['err_horaire_fin_invalide'] = 'E: L\'horaire de fin est invalide !';            
        }

   /* horaires */
   if( (int)$horaire_fin <= (int)$horaire_debut && $horaire_debut != -1 )
        {
            $erreurs['err_plage_horaire_invalide'] = 'E: L\'horaire de fin doit être supérieur à l\'horaire de début !';
        }
   
 

   /* categorie */
   if( !is_numeric($categorie) )
       {
           $erreurs['err_categorie_format'] = 'E:  La categorie est au mauvais format !';            
       }

   if( $categorie < 0 )
       {
           $erreurs['err_categorie_invalide'] = 'E:  La categorie est invalide !';            
       }

   /* libelle */
   if( $libelle !== strip_tags($libelle) )
       {
           $erreurs['err_libelle_html'] = 'E:  Balise html interdite !';
       }
   
    if( strlen($libelle) > 255 || $libelle === '' )
       {
           $erreurs['err_libelle_format'] = 'E:  Le libelle est au mauvais format (255 caractères maximum) !';            
       }

    return $erreurs;
}

function sd_bog_l_modif_rdv($id, $libelle, $date, $categorie, $horaire_debut, $horaire_fin)
{
    
    $erreurs = sd_bog_l_erreur($libelle, $date, $categorie, $horaire_debut, $horaire_fin);
    
    //insertion
    if( count($erreurs) === 0 )
        {
            sd_bog_bd_connexion();
            
            $S = 'UPDATE rendezvous SET
rdvDate = "'.mysqli_real_escape_string($GLOBALS['bd'], $date).'", 
rdvHeureDebut = "'.mysqli_real_escape_string($GLOBALS['bd'], $horaire_debut).'", 
rdvHeureFin = "'.mysqli_real_escape_string($GLOBALS['bd'], $horaire_fin).'", 
rdvLibelle = "'.mysqli_real_escape_string($GLOBALS['bd'], $libelle).'", 
rdvIDCategorie = "'.mysqli_real_escape_string($GLOBALS['bd'], $categorie).'", 
rdvIDUtilisateur = "'.mysqli_real_escape_string($GLOBALS['bd'], $_SESSION['id']).'" 

WHERE rdvID = "'.mysqli_real_escape_string($GLOBALS['bd'], $id).'"' ;

            $R = mysqli_query($GLOBALS['bd'], $S) or sd_bog_bd_erreur($S);

        }
    
    
    return $erreurs;  
}


function sd_bog_l_supprimer( $id )
{
    $erreurs = array();

    if( !is_numeric($id) || $id < 0 )
        {
            $erreurs['err_id_invalide'] = 'E: Le rendez-vous n\'existe pas !';
        }


    if( count($erreurs) === 0 )
        {
            sd_bog_bd_connexion();
            
            $S = 'DELETE FROM rendezvous WHERE
rdvID = "'.mysqli_real_escape_string($GLOBALS['bd'], $id).'"';

            $R = mysqli_query($GLOBALS['bd'], $S) or sd_bog_bd_erreur($S);   
        }
    
    return $erreurs;
    
}

?>