<?php

//TODO centrage formulaire
/**
 * Inscription d'un utilisateur
 *
 * @author : Sylvain DUMONTET
 * @author : Berenger OSSETE GOMBE
 */

// Bufferisation des sorties
ob_start();


// Inclusion de la bibliothéque
require('bibli_24sur7.php');
sd_bog_verifie_session();

echo '<!DOCTYPE HTML>';
echo '<html>';
sd_bog_html_head('24sur7 | Paramètres');
echo '<body>';
echo '<div id="carnet">';

sd_bog_html_bandeau(APP_PAGE_PARAMETRES);

echo '<main>';
echo '<section id="centre">';

// compte
if( !isset($_POST['btnMajCompte']) )
    {
        sd_bog_bd_connexion();
        $S = 'SELECT utiMail FROM utilisateur 
WHERE utiID = "'.mysqli_real_escape_string($GLOBALS['bd'], $_SESSION['id']).'" ';
        $R = mysqli_query($GLOBALS['bd'], $S) or sd_bog_erreur($S);

        $T = mysqli_fetch_assoc($R);
        
        $_POST['txtNom'] = $_SESSION['nom'];
        $_POST['txtMail'] = $T['utiMail'];
        $_POST['txtPasse'] = '';
        $_POST['txtVerif'] = '';
        
    }
else
    {
        $erreurs = sd_bog_l_modifier_compte($_POST['txtNom'], $_POST['txtMail'], $_POST['txtPasse'], $_POST['txtVerif']);

        //il y a des erreurs
        if( count($erreurs) > 0 )
            {
                foreach($erreurs as $e)
                    {
                        echo '<p>',$e,'</p>';
                    }
            }
        //pas d'erreur
        else
            {
                echo '<script language=javascript> alert("Vos informations ont bien été mises à jour !"); </script>';

                $_POST['txtNom'] = $_SESSION['nom'];
                $_POST['txtMail'] = '';
                $_POST['txtPasse'] = '';
                $_POST['txtVerif'] = '';
            }
    }

// calendrier
if( !isset($_POST['btnMajCalendrier']) )
    {
        
        sd_bog_bd_connexion();
        $S = 'SELECT utiHeureMin, utiHeureMax FROM utilisateur 
WHERE utiID = "'.mysqli_real_escape_string($GLOBALS['bd'], $_SESSION['id']).'" ';
        $R = mysqli_query($GLOBALS['bd'], $S) or sd_bog_erreur($S);

        $T = mysqli_fetch_assoc($R);
        
        $_POST['selHeureMin'] = $T['utiHeureMin'];
        $_POST['selHeureMax'] = $T['utiHeureMax'];

    
        
    }
else
    {
        
        $erreurs = sd_bog_l_modifier_calendrier($_POST['selHeureMin'], $_POST['selHeureMax'], 42);// TODO jour
        
        $erreurs = array();

        //il y a des erreurs
        if( count($erreurs) > 0 )
            {
                foreach($erreurs as $e)
                    {
                        echo '<p>',$e,'</p>';
                    }
            }
        //pas d'erreur
        else
            {
                echo '<script language=javascript> alert("Vos informations ont bien été mises à jour !"); </script>';
                
            }
    }

// CATEGORIE
//modification
foreach( $_POST as $k => $v )
    {
        if( strpos($k, 'btnModifierCategorie_') !== false  )
        {
            $erreurs = array();
            
            $id = (int)substr($k,strlen('btnModifierCategorie_'));
            $public = (isset($_POST['public_'.$id]));
            $erreurs = sd_bog_l_modifier_categorie($id, $_POST['txt_'.$id], $_POST['fond_'.$id], $_POST['bordure_'.$id], $public);

            //il y a des erreurs
            if( count($erreurs) > 0 )
                {
                    foreach($erreurs as $e)
                        {
                            echo '<p>',$e,'</p>';
                        }
                }

            $erreurs = array();
        
        }
    }



//creation
if( isset($_POST['btnAjouterNouvelleCategorie']) )
    {
        $erreurs = array();

        $erreurs = sd_bog_l_nouvelle_categorie($_POST['txt_0'], $_POST['fond_0'], $_POST['bordure_0'], isset($_POST['public_0']) );
         //il y a des erreurs
            if( count($erreurs) > 0 )
                {
                    foreach($erreurs as $e)
                        {
                            echo '<p>',$e,'</p>';
                        }
                }
            $erreurs = array();
    }
else
    {
        $_POST['txt_0'] = '';
        $_POST['fond_0'] = '';
        $_POST['bordure_0'] = '';
        $_POST['public_0'] = '';
    }


//supression
if( isset($_POST['btnConfirmer']) )
    {

        sd_bog_l_supprimer_categorie($_POST['hiddenIDCategorie']);
    }

    


/* AFFICHAGE */
//form compte
echo '<h2>Informations sur votre compte</h2>'; //TODO css pas beau orange
echo '<form method="POST" action="parametres.php">',
    '<table border="1" cellpadding="4" cellspacing="0">',
    sd_bog_form_ligne('Nom',
    sd_bog_form_input(APP_Z_TEXT, 'txtNom', $_POST['txtNom'], 20)),
    sd_bog_form_ligne('Email',
    sd_bog_form_input(APP_Z_TEXT, 'txtMail', $_POST['txtMail'], 20)),
    sd_bog_form_ligne('Mot de passe',
    sd_bog_form_input(APP_Z_PASS, 'txtPasse', $_POST['txtPasse'], 20)),
    sd_bog_form_ligne('Retapez votre mot de passe',
    sd_bog_form_input(APP_Z_PASS, 'txtVerif', $_POST['txtVerif'], 20)),

    sd_bog_form_ligne(sd_bog_form_input(APP_Z_SUBMIT, 'btnMajCompte', 'Mettre à jour'),

    sd_bog_form_input(APP_Z_RESET, 'btnAnnulerCompte', 'Annuler')),
    '</table></form>';

//form calendrier

echo '<h2>Option d\'affichage du calendrier</h2>'; //TODO css pas beau orange
echo '<form method="POST" action="parametres.php">',
    '<table border="1" cellpadding="4" cellspacing="0">',
    sd_bog_form_ligne('Jours affichés',
    sd_bog_form_checkbox('chkJours',
    array('Lundi','Mardi','Mercredi','Jeudi','Vendredi','Samedi','Dimanche'),
    array(true,true,true,true,true,true,true,true) )),
    sd_bog_form_ligne('Heure minimale',
    sd_bog_form_selection_horaire_fixe( 'selHeureMin', (int)$_POST['selHeureMin']) ),
    sd_bog_form_ligne('Heure maximale',
    sd_bog_form_selection_horaire_fixe( 'selHeureMax', (int)$_POST['selHeureMax']) ),
    sd_bog_form_ligne(sd_bog_form_input(APP_Z_SUBMIT, 'btnMajCalendrier', 'Mettre à jour'),
    sd_bog_form_input(APP_Z_RESET, 'btnAnnulerCalendrier', 'Annuler')),
    '</table></form>';



//form vos catégories
echo '<h2>Vos catégories</h2>'; //TODO css pas beau orange


//suppression confirmation
foreach( $_POST as $k => $v )
    {
        if( strpos($k, 'btnSupprimerCategorie_') !== false  )
        {
            $erreurs = array();
            
            $id = (int)substr($k,strlen('btnSupprimerCategorie_'));
            $public = (isset($_POST['public_'.$id]));

            $erreurs = sd_bog_l_supprimer_categorie_confirmation($id);

            //il y a des erreurs
            if( count($erreurs) > 0 )
                {
                    foreach($erreurs as $e)
                        {
                            echo '<p>',$e,'</p>';
                        }
                }

            $erreurs = array();
        
        }
    }

echo '<table border="1" cellpadding="4" cellspacing="0">',
    sd_bog_l_toutes_categories(),
    '</table>';
        
        
echo '<form method="POST" action="parametres.php">',
    '<table border="1" cellpadding="4" cellspacing="0">',
    '<th><td><h3>Nouvelle catégorie</h3></td></th>',
    sd_bog_l_form_ligne_categorie(0,$_POST['txt_0'],$_POST['fond_0'],$_POST['bordure_0'],isset($_POST['public_0'])),
    '</table></form>';

echo '</section>';
echo '</main>';

ob_end_flush();

sd_bog_html_pied();

echo '</div>'; //fermeture div carnet
echo '</body>';
echo '</html>';

/// FONCTIONS LOCALES
function sd_bog_l_toutes_categories()
{
    $res = '';
    sd_bog_bd_connexion();
    
    $S = '
SELECT *
FROM categorie, utilisateur
WHERE utiID = "'.mysqli_real_escape_string($GLOBALS['bd'], $_SESSION['id']).'" AND catIDUtilisateur = utiID
ORDER BY catNom
';
    $R = mysqli_query($GLOBALS['bd'], $S) or sd_bog_erreur($S);


    while( $T = mysqli_fetch_assoc($R) )
        {
            
            $res .=  '<form method="POST" action="parametres.php" class="formModifCategorie" >';
            $res .= sd_bog_l_form_ligne_categorie(
                htmlentities($T['catID']),
                htmlentities($T['catNom']),
                htmlentities($T['catCouleurFond']),
                htmlentities($T['catCouleurBordure']),
                (bool)htmlentities($T['catPublic']));

            $res .= '</form>';
            
        } 


    
    return $res;
}


function sd_bog_l_form_ligne_categorie($id=0, $nom='', $fond='', $bordure='', $public = false)
{
    $res = '<tr>';// TODO titres collés aux textboxs
	$res .= '<td> Nom</td><td>'.sd_bog_form_input(APP_Z_TEXT, 'txt_'.$id, $nom).'</td>';
    $res .= '<td> Fond</td><td>'.sd_bog_form_input(APP_Z_TEXT, 'fond_'.$id, $fond,10).'</td>';
    $res .= '<td> Bordure</td><td>'.sd_bog_form_input(APP_Z_TEXT, 'bordure_'.$id, $bordure,10).'</td>';
    $res .= '<td>'.sd_bog_form_checkbox('public_'.$id, array(''), array($public)).' </td><td>Public</td>';

    
    // nouvelle categorie
    if( $id === 0 )
        {
            $res .= '<td>'.sd_bog_form_input(APP_Z_SUBMIT, 'btnAjouterNouvelleCategorie', 'Ajouter').'</td>';
        }
    // categorie existante
    else
        {
            
            $style = '
background-color: #'.$fond.';
border: solid 2px #'.$bordure.';
';
            
            $res .= '<td><span class="apercuCategorie" style="'.$style.'">';
            $res .= 'Aperçu';
            $res .= '</span></td>';
            
            
            $res .= '<td>';

            $res .= '<input class="modifierCategorie" type="submit" name="btnModifierCategorie_'.$id.'" value="" >';// TODO ecriture zarbi

            $res .= '<input class="supprimerCategorie" type="submit" name="btnSupprimerCategorie_'.$id.'" value="" >';
            $res .= '</td>';


            
        }
    
    $res .= '</tr>';

    return $res;
}


function sd_bog_l_modifier_compte($txtNom, $txtMail, $txtPasse, $txtVerif)
{
    
    $erreurs = array();
    
    /*txt nom */
    $txtNom = trim($txtNom);
    if( strip_tags($txtNom) !== $txtNom )
        {
            $erreurs['err_nom_html'] = 'E:  Balise html interdite !';
        }
    $txtNom = htmlentities($txtNom);

    if($txtNom == '' ||  strlen($txtNom) > 30 ||  strlen($txtNom) < 4 )
        {
            $erreurs['err_nom_taille'] = 'Le nom doit avoir de 4 à 30 caractères';
        }

    /*txt mail */
    $txtMail = trim($txtMail);
    if( strip_tags($txtMail) !== $txtMail )
        {
            $erreurs['err_nom_html'] = 'E:  Balise html interdite !';
        }
    $txtMail = htmlentities($txtMail);

    if(strlen($txtMail) < 4
    || strpos($txtMail, '@') === FALSE
    || strpos($txtMail, '.') === FALSE )
        {
            $erreurs['err_mail_invalide'] = 'L\'adresse mail n\'est pas valide';
        }

    /*txt passe */
    $txtPasse = trim($txtPasse);
    if( strip_tags($txtPasse) !== $txtPasse )
        {
            $erreurs['err_nom_html'] = 'E:  Balise html interdite !';
        }
    $txtPasse = htmlentities($txtPasse);
    
    if(strlen($txtPasse) < 4 || strlen($txtPasse) > 20)
        {
            $erreurs['err_passe_invalide'] = 'Le mot de passe doit avoir de 4 à 20 caractères';
        }

    /*txt passe */ // TODO si pas de mdp : maj des autres
    $txtVerif = trim($txtVerif);
    if( strip_tags($txtVerif) !== $txtVerif )
        {
            $erreurs['err_nom_html'] = 'E:  Balise html interdite !';
        }
    $txtVerif = htmlentities($txtVerif);

    
    $txtVerif = trim($txtVerif);
    if ($txtPasse != $txtVerif) {
        $erreurs['err_passe_verif_different'] = 'Le mot de passe est différent dans les 2 zones';
    }
    
    if( count($erreurs) === 0 )
        {
             sd_bog_bd_connexion();
            
            $S = 'UPDATE utilisateur SET
utiNom = "'.mysqli_real_escape_string($GLOBALS['bd'], $txtNom).'", 
utiMail = "'.mysqli_real_escape_string($GLOBALS['bd'], $txtMail).'", 
utiPasse = "'.mysqli_real_escape_string($GLOBALS['bd'], md5($txtPasse)).'"

WHERE utiID = "'.mysqli_real_escape_string($GLOBALS['bd'], $_SESSION['id']).'"' ;

            $R = mysqli_query($GLOBALS['bd'], $S) or sd_bog_bd_erreur($S);
            $_SESSION['nom'] = $txtNom;



        }

    return $erreurs;
}

function sd_bog_l_modifier_calendrier($heure_min, $heure_max,$jour)
{
    $erreurs = array();

    if( $heure_min < 0 || $heure_min > 23 )
        {
            $erreurs['err_heure_debut_invalide'] = 'E: L\'heure de début doit être comprise entre 0:00 et 23:00 !';
        }

    if( $heure_max < 0 || $heure_max > 23 )
        {
            $erreurs['err_heure_fin_invalide'] = 'E: L\'heure de fin doit être comprise entre 0:00 et 23:00 !';
        }
    
    if( $heure_min >= $heure_max )
        {
            $erreurs['err_plage_horaire_invalide'] = 'E: L\'heure de début doit être inférieure à l\'heure de fin !';
        }


    if( count($erreurs) === 0 )
        {
            sd_bog_bd_connexion();
            
            $S = 'UPDATE utilisateur SET
utiHeureMin = "'.mysqli_real_escape_string($GLOBALS['bd'], $heure_min).'", 
utiHeureMax = "'.mysqli_real_escape_string($GLOBALS['bd'], $heure_max).'"
WHERE utiID = "'.mysqli_real_escape_string($GLOBALS['bd'], $_SESSION['id']).'"' ;

            $R = mysqli_query($GLOBALS['bd'], $S) or sd_bog_bd_erreur($S);
        }

    return $erreurs;
}

function sd_bog_l_is_hexa($n)
{
    $lower = strtolower($n);

    for($i = 0, $max = strlen($lower); $i < $max; $i++)
        {
            if( ( $lower[$i] < 'a' || $lower[$i] > 'f' )
            && ( $lower[$i] < '0' || $lower[$i] > '9' ) )
                {
                    return false;
                }
        }

    return true;
}

function sd_bog_l_erreur_categorie($id, $nom, $fond, $bordure, $public)
{
    $erreurs = array();
    
    /* nom */
    $nom = trim($nom);
    
    if( strip_tags($nom) !== $nom )
        {
            $erreurs['err_nom_html'] = 'E:  Balise html interdite !';
        }
    $nom = htmlentities($nom);


    if( $nom === '')
        {
            $erreurs['err_nom_absent'] = 'E:  Le nom de la catégorie doit être renseigné';
        }
        
    if( strlen($nom) < 4 || strlen($nom) > 20)
        {
            $erreurs['err_nom_format'] = 'E:  Le nom de la catégorie doit être composé de 4 à 20 caractères !';
        }
    
    /* fond */
    $fond = trim($fond);
    if( strip_tags($fond) !== $fond )
        {
            $erreurs['err_fond_html'] = 'E:  Balise html interdite !';
        }
    $fond = htmlentities($fond);

    if( !sd_bog_l_is_hexa($fond) )
        {
            $erreurs['err_fond_hexa'] = 'E:  Les couleurs du fond doivent être en hexadécimal !';
        }

    if( strlen($fond) !== 6 )
        {
            $erreurs['err_fond_format'] = 'E:  Les couleurs du fond doivent être sur 6 caractères !';
        }

    /* bordure */
    $bordure = trim($bordure);
    if( strip_tags($bordure) !== $bordure )
        {
            $erreurs['err_bordure_html'] = 'E:  Balise html interdite !';
        }
    $bordure = htmlentities($bordure);

    if( !sd_bog_l_is_hexa($bordure) )
        {
            $erreurs['err_bordure_hexa'] = 'E:  La couleur des bordures doivent être en hexadécimal';
        }

    if( strlen($bordure) !== 6 )
        {
            $erreurs['err_bordure_format'] = 'E:  La couleur des bordures doivent être  sur 6 caractères !';
        }

    /* public */
    $public = trim($public);
    $public = htmlentities($public);
    if( $public != 0 && $public != 1 )
        {
            $erreurs['err_public_invalide'] = 'E: Attribut public mal renseigné !';
        }
    
    return $erreurs;
}

function sd_bog_l_modifier_categorie($id, $nom, $fond, $bordure, $public)
{
    $erreurs = sd_bog_l_erreur_categorie($id, $nom, $fond, $bordure, $public);

    if( count($erreurs) === 0 )
        {
            sd_bog_bd_connexion();
            
            $S = 'UPDATE categorie SET
catNom = "'.mysqli_real_escape_string($GLOBALS['bd'], $nom).'", 
catCouleurFond = "'.mysqli_real_escape_string($GLOBALS['bd'], $fond).'", 
catCouleurBordure = "'.mysqli_real_escape_string($GLOBALS['bd'], $bordure).'", 
catPublic = "'.mysqli_real_escape_string($GLOBALS['bd'], $public).'"
WHERE catID = "'.mysqli_real_escape_string($GLOBALS['bd'], $id).'"
AND catIDUtilisateur = "'.mysqli_real_escape_string($GLOBALS['bd'], $_SESSION['id']).'"' ;

$R = mysqli_query($GLOBALS['bd'], $S) or sd_bog_bd_erreur($S);
        }

    return $erreurs;
}

function sd_bog_l_nouvelle_categorie($nom, $fond, $bordure, $public)
{
     $erreurs = sd_bog_l_erreur_categorie(0, $nom, $fond, $bordure, $public);

     if( count($erreurs) === 0 )
         {
             sd_bog_bd_connexion();
             
             $S = ' INSERT INTO categorie(catNom, catCouleurFond, catCouleurBordure, catIDUtilisateur, catPublic) 
VALUES(\''.mysqli_real_escape_string($GLOBALS['bd'], $nom).'\',
\''.mysqli_real_escape_string($GLOBALS['bd'], $fond).'\',
\''.mysqli_real_escape_string($GLOBALS['bd'], $bordure).'\',
\''.mysqli_real_escape_string($GLOBALS['bd'], $_SESSION['id']).'\',
\''.mysqli_real_escape_string($GLOBALS['bd'], (int)($public)).'\')
';
             
             $R = mysqli_query( $GLOBALS['bd'], $S) or sd_bog_bd_erreur($S);
             
         }

     return $erreurs;
}

function sd_bog_l_supprimer_categorie_confirmation($id)
{

     echo  '<form method="POST" action="parametres.php" class="formModifCategorie" >';
     echo '<div id="confirmerSuppressionCategorie">';
     
     echo '<span>Supprimer la catégorie et les rendez-vous et événements associés ?</span>';
     
     echo '<input type="submit" name = "btnConfirmer" value="Confirmer" /> ';

     echo '<input type="hidden" name="hiddenIDCategorie" value="'.$id.'" >';
     
     echo '</div>';

     echo '</form>';

    
}

function sd_bog_l_supprimer_categorie($id)
{
    sd_bog_bd_connexion();
             
    $S = 'DELETE FROM categorie
WHERE catID = "'.mysqli_real_escape_string($GLOBALS['bd'], $id).'"';
             
    $R = mysqli_query( $GLOBALS['bd'], $S) or sd_bog_bd_erreur($S);

}

?>