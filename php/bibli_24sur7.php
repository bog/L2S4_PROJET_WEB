<?php
/** @file
 * Bibliothèque générale de fonctions
 *
 * @author : Sylvain DUMONTET
 * @author : Berenger OSSETE GOMBE
 */

//____________________________________________________________________________
//
// Défintion des constantes de l'application
//____________________________________________________________________________

define('APP_TEST', 'true');

// Gestion des infos base de données
define('APP_BD_URL', 'localhost');
define('APP_BD_USER', 'u_24sur7');
define('APP_BD_PASS', 'p_24sur7');
define('APP_BD_NOM', '24sur7');

define('APP_NOM_APPLICATION','24sur7');

// Gestion des pages de l'application
define('APP_PAGE_AGENDA', 'agenda.php');
define('APP_PAGE_RECHERCHE', 'recherche.php');
define('APP_PAGE_ABONNEMENTS', 'abonnements.php');
define('APP_PAGE_PARAMETRES', 'parametres.php');

// Définition du nom des mois. Comme une constante
// ne peut être que scalaire, on utilise une chaîne
// qu'on "explodera" en tableau pour l'utiliser
define('APP_MOIS', 	'x,Janvier,Février,Mars,Avril,Mai,Juin,Juillet,Août,Septembre,Octobre,Novembre,Décembre');

// Définition des types de zones de saisies
define('APP_Z_TEXT', 'text');
define('APP_Z_PASS', 'password');
define('APP_Z_SUBMIT', 'submit');
define('APP_Z_RESET', 'reset');

//____________________________________________________________________________


/**
 * Connexion à la base de données.
 * Le connecteur obtenu par la connexion est stocké dans une
 * variable global : $GLOBALS['bd']
 * Le connecteur sera ainsi accessible partout.
 */
function sd_bog_bd_connexion() {
	$bd = mysqli_connect(APP_BD_URL, APP_BD_USER, APP_BD_PASS, APP_BD_NOM);

	if ($bd !== FALSE) {
		$GLOBALS['bd'] = $bd;
		return;					// Sortie connexion OK
	}

	// Erreur de connexion
	// Collecte des informations facilitant le debugage
	$msg = '<h4>Erreur de connexion base MySQL</h4>'
        .'<div style="margin: 20px auto; width: 350px;">'
        .'APP_BD_URL : '.APP_BD_URL
        .'<br>APP_BD_USER : '.APP_BD_USER
        .'<br>APP_BD_PASS : '.APP_BD_PASS
        .'<br>APP_BD_NOM : '.APP_BD_NOM
        .'<p>Erreur MySQL num&eacute;ro : '.mysqli_connect_errno($bd)
                                           .'<br>'.mysqli_connect_error($bd)
                                                  .'</div>';

	sd_bog_bd_erreurExit($msg);
}

//____________________________________________________________________________

/**
 * Traitement erreur mysql, affichage et exit.
 *
 * @param string		$sql	Requête SQL ou message
 */
function sd_bog_bd_erreur($sql) {
	$errNum = mysqli_errno($GLOBALS['bd']);
	$errTxt = mysqli_error($GLOBALS['bd']);

	// Collecte des informations facilitant le debugage
	$msg = '<h4>Erreur de requ&ecirc;te</h4>'
        ."<pre><b>Erreur mysql :</b> $errNum"
        ."<br> $errTxt"
        ."<br><br><b>Requ&ecirc;te :</b><br> $sql"
        .'<br><br><b>Pile des appels de fonction</b>';

	// Récupération de la pile des appels de fonction
	$msg .= '<table border="1" cellspacing="0" cellpadding="2">'
        .'<tr><td>Fonction</td><td>Appel&eacute;e ligne</td>'
        .'<td>Fichier</td></tr>';

	// http://www.php.net/manual/fr/function.debug-backtrace.php
	$appels = debug_backtrace();
	for ($i = 0, $iMax = count($appels); $i < $iMax; $i++) {
		$msg .= '<tr align="center"><td>'
            .$appels[$i]['function'].'</td><td>'
            .$appels[$i]['line'].'</td><td>'
            .$appels[$i]['file'].'</td></tr>';
	}

	$msg .= '</table></pre>';

	sd_bog_bd_erreurExit($msg);
}

//___________________________________________________________________
/**
 * Arrêt du script si erreur base de données.
 * Affichage d'un message d'erreur si on est en phase de
 * développement, sinon stockage dans un fichier log.
 *
 * @param string	$msg	Message affiché ou stocké.
 */
function sd_bog_bd_erreurExit($msg) {
	ob_end_clean();		// Supression de tout ce qui a pu être déja généré

	// Si on est en phase de développement, on affiche le message
	if (APP_TEST) {
		echo '<!DOCTYPE html><html><head><meta charset="UTF-8"><title>',
            'Erreur base de données</title></head><body>',
            $msg,
            '</body></html>';
		exit();
	}

	// Si on est en phase de production on stocke les
	// informations de débuggage dans un fichier d'erreurs
	// et on affiche un message sibyllin.
	$buffer = date('d/m/Y H:i:s')."\n$msg\n";
	error_log($buffer, 3, 'erreurs_bd.txt');

	// Génération d'une page spéciale erreur
	sd_bog_html_head('24sur7');

	echo '<h1>24sur7 est overbook&eacute;</h1>',
        '<div id="bcDescription">',
        '<h3 class="gauche">Merci de r&eacute;essayez dans un moment</h3>',
        '</div>';

	sd_bog_html_pied();

	exit();
}
//____________________________________________________________________________




/**
 * Génère le code HTML du début des pages.
 *
 * @param string	$titre		Titre de la page
 * @param string	$css		url de la feuille de styles liée
 */
function sd_bog_html_head($titre, $css = '../css/style.css') {
	
    echo '<head>',
        '<meta charset="UTF-8">',
        '<title>', $titre, '</title>',
        '<link rel="stylesheet" href="', $css, '">',
        '<link rel="shortcut icon" href="../images/favicon.ico" type="image/x-icon">',
        '</head>';

}



//____________________________________________________________________________

/**
 * Génère le code HTML du bandeau des pages.
 *
 * @param string	$page		Constante APP_PAGE_xxx
 */
function sd_bog_html_bandeau($page)
{
	echo '<header>',
        '<nav id="bcOnglets">';
    
    echo ($page == APP_PAGE_AGENDA) ? '<span class="off">Agenda</span>' : '<a href="'.APP_PAGE_AGENDA.'">Agenda</a>',
        ($page == APP_PAGE_RECHERCHE) ? '<span class="off">Recherche</span>' : '<a href="'.APP_PAGE_RECHERCHE.'">Recherche</a>',
        ($page == APP_PAGE_ABONNEMENTS) ? '<span class="off">Abonnements</span>' : '<a href="'.APP_PAGE_ABONNEMENTS.'">Abonnements</a>',
        ($page == APP_PAGE_PARAMETRES) ? '<span class="off">Paramètres</span>' : '<a href="'.APP_PAGE_PARAMETRES.'">Paramètres</a>';
    
    echo '</nav>',
        '<div id="bcLogo"></div>',
        '<a href="deconnexion.php" id="btnDeconnexion" title="Se déconnecter"></a>',
        '</header>';
}

/**
 * Génère le code HTML du bandeau des pages sans onglet.
 */
function sd_bog_html_bandeau_sans_onglet()
{
	echo '<header>',
        '<nav id="bcOnglets">','</nav>',
        '<div id="bcLogo"></div>',
        '<a href="deconnexion.php" id="btnDeconnexion" title="Se déconnecter"></a>',
        '</header>';
}

//____________________________________________________________________________

/**
 * Génère le code HTML du pied des pages.
 */
function sd_bog_html_pied() {
	echo '<footer>',
        '<a id="apropos" href="#">A propos</a>',
        '<a id="confident" href="#">Confidentialité</a>',
        '<a id="conditions" href="#">Conditions</a>',
        '<p id="copyright">24sur7 & Partners &copy; 2012</p>',
		'</footer>';

}

//____________________________________________________________________________



//_______________________________________________________________

/**
 * Renvoie le nom d'un mois.
 *
 * @param integer	$numero		Numéro du mois (entre 1 et 12)
 *
 * @return string 	Nom du mois correspondant
 */
function sd_bog_get_mois($numero) {
	$numero = (int) $numero;
	($numero < 1 || $numero > 12) && $numero = 0;

	$mois = explode(',', APP_MOIS);

	return $mois[$numero];
}

//____________________________________________________________________________

/**
 * Formatte une date AAAAMMJJ en format lisible
 *
 * @param integer	$amj		Date au format AAAAMMJJ
 *
 * @return string	Date formattée JJ nomMois AAAA
 */
function sd_bog_date_claire($amj) {
	$a = (int) substr($amj, 0, 4);
	$m = (int) substr($amj, 4, 2);
	$m = sd_bog_get_mois($m);
	$j = (int) substr($amj, -2);

	return "$j $m $a";
}

//____________________________________________________________________________

/**
 * Formatte une heure HHMM en format lisible
 *
 * @param integer	$h		Heure au format HHMM
 *
 * @return string	Heure formattée HH h SS
 */
function sd_bog_heure_claire($h) {
	$m = (int) substr($h, -2);
	($m == 0) && $m = '';
	$h = (int) ($h / 100);

	return "{$h}h{$m}";
}

//____________________________________________________________________________

/**
 * Redirige l'utilisateur sur une page
 *
 * @param string	$page		Page où rediriger
 */
function sd_bog_redirige($page) {
	header("Location: $page");
	exit();
}


//_______________________________________________________________
/**
 * Véfication d'une session.
 *
 * Redirection sur la page d'inscription si la session n'est pas ouverte.
 */
function sd_bog_verifie_session() {
	session_start();
	if (! isset($_SESSION['id'])) {
		session_destroy();
		header('Location: identification.php');
		exit();
	}
}
//_______________________________________________________________

/**
 * Génére le code HTML d'une ligne de tableau d'un formulaire.
 *
 * Les formulaires sont mis en page avec un tableau : 1 ligne par
 * zone de saisie, avec dans la collone de gauche le lable et dans
 * la colonne de droite la zone de saisie.
 *
 * @param string		$gauche		Contenu de la colonne de gauche
 * @param string		$droite		Contenu de la colonne de droite
 *
 * @return string	Le code HTML de la ligne du tableau
 */
function sd_bog_form_ligne($gauche, $droite)
{
	return "<tr><td>{$gauche}</td><td>{$droite}</td></tr>";
}



//_______________________________________________________________

/**
 * Génére le code d'une zone input de formulaire (type text, password ou button)
 *
 * @param string		$type	le type de l'input (constante SD_BOG_Z_xxx)
 * @param string		$name	Le nom de l'input
 * @param String		$value	La valeur par défaut
 * @param integer	$size	La taille de l'input
 *
 * @return string	Le code HTML de la zone de formulaire
 */
function sd_bog_form_input($type, $name, $value, $size=0) {
	$value = htmlentities($value, ENT_COMPAT, 'UTF-8');
	$size = ($size == 0) ? '' : "size='{$size}'";
	return "<input type='{$type}' name='{$name}' {$size} value=\"{$value}\">";
}

/**
 * Génére le code d'une zone input de formulaire (type text, password ou button)
 *
 * @param string		$type	le type de l'input (constante SD_BOG_Z_xxx)
 * @param string		$name	Le nom de l'input
 * @param String		$value	La valeur par défaut
 * @param integer	$size	La taille de l'input
 *
 * @return string	Le code HTML de la zone de formulaire
 */
function sd_bog_form_checkbox($name, $values, $checked)
{
    $res = '';
    $i=0;

    foreach( $values as $v )
        {
            $res .= ' <input type = "checkbox" name = "'.$name.'"';
            $res .= 'value = "'.$v.'" ';
            
            if( isset($checked[$i]) && $checked[$i] === true )
                {
                    $res .= ' checked = "checked" ';
                }
            $res .= '>';
            $res .= '<span>'.$v.'</span>';

            if($i % 3 === 2)
                {
                     $res .= '</br>';
                }
            
            $i++;
        }

    return $res;
}

//_______________________________________________________________

/**
 * Génére le code pour un ensemble de trois zones de sélection
 * représentant une date : jours, mois et années
 *
 * @param string		$nom	Préfixe pour les noms des zones
 * @param integer	$jour 	Le jour sélectionné par défaut
 * @param integer	$mois 	Le mois sélectionné par défaut
 * @param integer	$annee	l'année sélectionnée par défaut
 *
 * @return string 	Le code HTML des 3 zones de liste
 */
function sd_bog_form_date($nom, $jour = 0, $mois = 0, $annee = 0) {
	list($AA, $MM, $JJ) = explode('-', date('Y-n-j'));
	($jour == 0) && $jour = $JJ;
	($mois == 0) && $mois = $MM;
	($annee == 0) && $annee = $AA;

	$H = "<select name='{$nom}_j'>";
	for ($i = 1; $i < 32; $i++) {
		$selected = ($i == $jour) ? ' selected' : '';
		$H .= "<option value='{$i}'{$selected}>{$i}";
	}
	$H .= '</select>';

	$libMois = explode(',', APP_MOIS);

	$H .= "<select name='{$nom}_m'>";
	for ($i = 1; $i < 13; $i++) {
		$selected = ($i == $mois) ? ' selected' : '';
		$H .= "<option value='{$i}'{$selected}>{$libMois[$i]}";
	}
	$H .= '</select>';

	$H .= "<select name='{$nom}_a'>";
	for ($i = $AA, $iMin = $AA - 99; $i >= $iMin; $i--) {
		$selected = ($i == $annee) ? ' selected' : '';
		$H .= "<option value='{$i}'{$selected}>{$i}";
	}
	$H .= '</select>';

	return $H;
}

function sd_bog_form_selection_horaire_fixe($nom, $heure = 0)
{
    $res = '<select name ='.$nom.'>';
    for( $h = 0; $h < 24; $h++ )
        {
            $res .= '<option value="'.$h.'" ';
            
            if( $h === $heure ){ $res .= ' selected' ;}
            $res .= '>' ;

            $res .= $h.':00';
            
            $res .= '</option>';
        }
    $res .= '</select>';

    return $res;
}

/**
 * Génére le code pour une horaire en formulaire
 * représentant une liste de categories
 *
 * @param string		$nom	Nom de la zone de selection
 *
 * @return string 	Le code HTML de la liste
 */
function sd_bog_form_selection_horaire($nom, $heure= 0, $minute = 0)
{
    $res = '<select name ='.$nom.'_heure>';
    for( $h = 0; $h < 24; $h++ )
        {
            $res .= '<option value="'.$h.'" ';
            
            if( $h === $heure ){ $res .= ' selected' ;}
            $res .= '>' ;

            $res .= $h;
            
            $res .= '</option>';
        }
    $res .= '</select>';

    $res .= '<select name ='.$nom.'_minute>';
    for( $m = 0; $m < 60; $m++ )
        {
            $res .= '<option value="'.$m.'" ';
            
            if( $m === $minute ){ $res .= ' selected' ;}
            $res .= '>' ;

            if( $m < 10 ){ $res .= '0';}
                
            $res .= $m;
            
            $res .= '</option>';
        }
    
    $res .= '</select>';
    
    return $res;
}

/**
 * Génére le code pour une zones de sélection
 * représentant une liste de categories
 *
 * @param string		$nom	Nom de la zone de selection
 *
 * @return string 	Le code HTML de la liste
 */
function sd_bog_form_selection_categories($nom)
{

    ob_start();
    sd_bog_bd_connexion();
    
    $res = '<select name ='.$nom.'>';
    
    
    $S = 'SELECT * FROM utilisateur, categorie
       WHERE utiID = "'.htmlentities($_SESSION['id']).'" 
AND catIDUtilisateur = utiID';
    
    $R = mysqli_query($GLOBALS['bd'], $S) or sd_bog_bd_erreur($S);
    $i = 1;
    
    while( $T = mysqli_fetch_assoc($R) )
        {
            $res .= '<option value="'.htmlentities($T['catID']).' " >';
            $res .= htmlentities($T['catNom']);
            $res .= '</option>';
            $i++;
        }
    
    $res .= '</select>';

    mysqli_close($GLOBALS['bd']);
    mysqli_free_result($R);
    
    ob_end_flush();
    return $res;
}



function sd_bog_html_categorie()
{
    ob_start();
    sd_bog_bd_connexion();
    
    echo  ' <section id="categories">
	    <h3>Vos agendas</h3>
	    <p><a href="#">Agenda de '.$_SESSION['nom'].'</a></p>';
        
        echo '<ul>';
        
         $S = 'SELECT * FROM utilisateur, categorie
       WHERE utiID = "'.htmlentities($_SESSION['id']).'" 
AND catIDUtilisateur = utiID';

         /*
       AND rdvDate >= "'.htmlentities($lundi).'"
 AND rdvDate <= "'.htmlentities($dimanche).'" 
 AND rdvIDCategorie = catID';*/ // TODO ciblage semaine uniquement

         $R = mysqli_query($GLOBALS['bd'], $S) or sd_bog_bd_erreur($S);

         while( $T = mysqli_fetch_assoc($R) )
             {
                 $style_categorie = '
border: solid 2px #'.$T['catCouleurBordure'].'; 
    background-color: #'.$T['catCouleurFond'].';
';
        
                 echo '<li><div class="categorie_legende" style="'.$style_categorie.'"></div>'.$T['catNom'].'</li>';
             }
         
        echo '</ul>';
    
        echo '</section>';



        //fermeture bd
        mysqli_close($GLOBALS['bd']);
        mysqli_free_result($R);
    
}



/**
 * Génère le code HTML d'un calendrier.
 *
 * @param integer	$jour		Numéro du jour à afficher
 * @param integer	$mois		Numéro du mois à afficher
 * @param integer	$annee		Année à afficher
 */
function sd_bog_html_calendrier($jour=0, $mois=0, $annee=0){
	// vérification des paramètres 
	$param_defaut = false;
	$t=time();
	if ((!checkdate($mois, $jour, $annee))|| ($annee<2012)) {
		$jour= date('j',$t);
		$mois= date('n',$t);
		$annee= date('Y',$t);
	}

	// tableau des jours
	$nom_jour = array('dimanche', 'lundi', 'mardi', 'mercredi', 'jeudi', 'vendredi', 'samedi');

	// tableau des mois
	$nom_mois = array('', 'janvier', 'f&eacute;vrier', 'mars', 'avril', 'mai', 'juin', 'juillet', 'ao&ucirc;t', 'septembre', 'octobre', 'novembre', 'd&eacute;cembre');

	// titre calendrier + en-tête tableau
	echo
		'<section id="calendrier">',
		'<p>',
		'<a href="#" class="flechegauche"><img src="../images/fleche_gauche.png" alt="picto fleche gauche"></a>',
		$nom_mois[(int)$mois],' ',$annee,
		'<a href="#" class="flechedroite"><img src="../images/fleche_droite.png" alt="picto fleche droite"></a>',
		'</p>',
		'<table>',
		'<tr>',
		'<th>Lu</th><th>Ma</th><th>Me</th><th>Je</th><th>Ve</th><th>Sa</th><th>Di</th>',
		'</tr>';

	// détermine les variables necessitant un appel à date()
	$premier_j=date('N',mktime(0,0,0,$mois,1,$annee));
	$nbj_mois_courant=date('t',mktime(0,0,0,$mois,$jour,$annee));
	$nbj_mois_avant=date('t',mktime(0,0,0,$mois-1,1,$annee));
	$aujourdhui=mktime(0,0,0,date('n',$t),date('j',$t),date('Y',$t));
	$jour_select=mktime(0,0,0,$mois,$jour,$annee);
	$semaine_select=date('W',mktime(0,0,0,$mois,$jour,$annee));

	// compte le nombre de lignes necessaires dans le calendrier
	if ((($nbj_mois_courant+$premier_j-1)/7)<=5) {
		$nb_lignes=5;
	}
	else {
		$nb_lignes=6;
	}

// Ecrit le contenu du tableau
	for ($i=0;$i<$nb_lignes;$i++){
		if ($semaine_select==date('W',mktime(0,0,0,$mois,(1+$i*7),$annee))) {
			echo '<tr class="semaineCourante">';
		}
		else {
			echo'<tr>';
		}
		for ($j=1;$j<8;$j++){
			if (($i*7+$j)<$premier_j) { // si case dans le mois précédent
                $numJour = $nbj_mois_avant-$premier_j+$j+1;
                $numMois = $mois -1;
                if( $numMois === 0 ){$numMois = 12;}
                $numAnnee = $annee;
                if($mois === 1){$numAnnee = $annee - 1;}
                
				echo'<td><a class="lienJourHorsMois" href="agenda.php?j='.$numJour.'&m='.$numMois.'&a='.$numAnnee.'">',$numJour,'</a></td>';

			}
			else {
				if (($i*7+$j)>=($premier_j+$nbj_mois_courant)) { // si case dans le mois suivant
                    $numJour = ($i*7+$j-$nbj_mois_courant-$premier_j+1);
                    $numMois = $mois+1;
                    if( $numMois === 13 ){$numMois = 1;}
                    $numAnnee = $annee;
                    if($mois === 12){$numAnnee = $annee +1;}
                
					echo'<td><a class="lienJourHorsMois" href="agenda.php?j='.$numJour.'&m='.$numMois.'&a='.$numAnnee.'">',($i*7+$j-$nbj_mois_courant-$premier_j+1),'</a></td>';
				}
				else {
					if ($aujourdhui==mktime(0,0,0,$mois,($i*7+$j-$premier_j+1),$annee)) { // si case correspond à aujourd'hui
						echo'<td><a class="aujourdHui" href="agenda.php" >',($i*7+$j-$premier_j+1),'</a></td>';
					}
					else {
						if (mktime(0,0,0,$mois,($i*7+$j-$premier_j+1),$annee)==$jour_select) { // si case correspond au jour sélectionné
							echo'<td><a class="jourCourant">',($i*7+$j-$premier_j+1),'</a></td>';
                            
						}
						else { // sinon (case correspond au mois courant, mais pas à aujourd'hui ni au jour sélectionné)
                            $numJour = ($i*7+$j-$premier_j+1);
                            $numMois = $mois;
                            $numAnnee = $annee;
							echo'<td><a href="agenda.php?j='.$numJour.'&m='.$numMois.'&a='.$numAnnee.'">',$numJour,'</a></td>';
						}
					} 
				}
			}
		}
		echo'</tr>';
	}
	echo
		'</table>',
		'</section>';
}


?>